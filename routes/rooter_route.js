const express = require('express');
const rooter = express.Router();
const Tache = require('../models/Tache_model');

rooter.set("view engine", "ejs");
rooter.set("views", __dirname + "/views");
rooter.use('/assets', express.static('public'))
rooter.set("view options", { layout: false } );

rooter.use(bodyParser.json());      
rooter.use(bodyParser.urlencoded({extended: true}));

rooter.use(cookieParser());
rooter.use(session({
	secret: 'keyboard cat', 
	resave: false,
  	saveUninitialized: true}
));

rooter.use(require('./middlewares/flash.js'));


rooter.get('', function(req, res) {
	Tache.save()
	.then(data => {

	})
	.catch(err=>{

	});
	res.render('index', {listes : req.session.list});
});

rooter.get('/supprimer/:tache', function(req, res) {
	for (var i = 0; i < req.session.list.length; i++) {
		if (req.session.list[i] === req.params.tache) {
			req.session.list.splice(i, 1);
			req.flash('suppression', 'Tache supprimee avec succes');
		}
	}
	
	res.redirect('/');
					  
});

rooter.post('/modifier', function(req, res) {
	console.log(req.body.ancienneTacheName)
	console.log(req.body.nouvelleTacheName)

	var index = req.session.list.indexOf(req.body.ancienneTacheName);
	if (index !== -1) {
		req.session.list[index] = req.body.nouvelleTacheName;
	}
	res.json({ nouveau: req.body.nouvelleTacheName });
					  
});

rooter.post('/envoyer', function (req, res) {

	 if (!(req.body.afaire === '')){
		 const tache = new Tache({
			title : req.body.afaire,
			date : Date.now
		 });
	 	// if (req.session.list) {
	 	// 	req.session.list.push(req.body.afaire);
	 	// }else{
	 	// 	req.session.list = [req.body.afaire]
	 	// }
	 	req.flash('success', 'Tache bien envoyee');
	 }else{
	 	req.flash('erreur', 'Aucune tache envoyee');
	 }
	res.redirect('/');
	
})

module.exports = rooter;
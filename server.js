var express = require('express');
var session = require('express-session');
var flash = require('connect-flash')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser');
var Tache = require('./models/Tache_model.js')
const mongoose = require('mongoose');
require('dotenv/config');
var app = express();

app.set("view engine", "ejs");
app.set("views", __dirname + "/views");
app.use('/assets', express.static('public'))
app.set("view options", { layout: false } );

app.use(bodyParser.json());      
app.use(bodyParser.urlencoded({extended: true}));

app.use(cookieParser());
app.use(session({
	secret: 'keyboard cat', 
	resave: false,
  	saveUninitialized: true}
));

app.use(require('./middlewares/flash.js'));

app.get('', function(req, res) {
	Tache.find({}, (err, tachesList)=>{
		if(err) throw err;
		res.render('index', {listes : tachesList});
	})
});
app.get('/supprimer/:id', function(req, res) {
	Tache.deleteOne({ _id: req.params.id }, function (err) {
		if (err) throw (err);
		req.flash('suppression', 'Tache supprimee avec succes');
		res.redirect('/');
		// deleted at most one tank document
	  });
	// for (var i = 0; i < req.session.list.length; i++) {
	// 	if (req.session.list[i] === req.params.tache) {
	// 		req.session.list.splice(i, 1);
	// 		req.flash('suppression', 'Tache supprimee avec succes');
	// 	}
	// }
	
					  
});

app.post('/modifier', function(req, res) {
	console.log(req.body.ancienneTacheName)
	console.log(req.body.nouvelleTacheName)
	Tache.findOneAndUpdate(
		{title : req.body.ancienneTacheName},
		{title : req.body.nouvelleTacheName}, {
			new : true,
			upsert: true
		});

	// var index = req.session.list.indexOf(req.body.ancienneTacheName);
	// if (index !== -1) {
	// 	req.session.list[index] = req.body.nouvelleTacheName;
	// }
	res.json({ nouveau: req.body.nouvelleTacheName });
					  
});

app.post('/envoyer', function (req, res) {
	if (!(req.body.afaire === '')){
		Tache.findOne({title : req.body.afaire},
			(err, tacheTrouve)=>{
				if(err) throw err;
				console.log(tacheTrouve);
				if(tacheTrouve !== null){
					req.flash('erreur', 'Tache deja existante');
					res.redirect('/');
				}else{
					const tache = new Tache({
						title : req.body.afaire
					 });
					 tache.save((err)=>{
						 if(err) throw err;
						 req.flash('success', 'Tache bien envoyee');
						 res.redirect('/');
					 });
				}
			});
		
	}else{
		req.flash('erreur', 'Aucune tache envoyee');
		res.redirect('/');
	}
	//  if (!(req.body.afaire === '')){
	//  	if (req.session.list) {
	//  		req.session.list.push(req.body.afaire);
	//  	}else{
	//  		req.session.list = [req.body.afaire]
	//  	}
	//  	req.flash('success', 'Tache bien envoyee');
	//  }else{
	//  	req.flash('erreur', 'Aucune tache envoyee');
	//  }
	
	
	
})

// app.use(function (rew, res, next) {
// 	res.redirect('/');
// })
	mongoose.connect(
		process.env.DB_CONNECTION, 
	{ useNewUrlParser: true }, ()=>{
		console.log('connected to db');
	});
	app.listen(8080, function () {
	console.log('roule actuellement sur le port 8080');
	});



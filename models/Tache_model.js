const mongoose = require('mongoose');

const TacheSchema = mongoose.Schema({
    title : {
        type: String,
        required: true
    },
    date : {
        type : Date,
        default :Date.now
    }
})

module.exports = mongoose.model('Tache', TacheSchema);